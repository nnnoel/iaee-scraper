__author__ = 'noel'

from bs4 import BeautifulSoup
from urllib2 import urlopen
import re
import csv

# _url = 'https://members.iaee.com/cvweb/cgi-bin/memberdll.dll/List?SORT=LASTNAME%2C+FIRSTNAME&ISMEMBERFLG_field=on&ISMEMBERFLG=Y&NOWEBFLG=%3C%3EY&SHOWSQL=N'
# _url = 'https://members.iaee.com/cvweb/cgi-bin/memberdll.dll/List?SORT=LASTNAME%2C+FIRSTNAME&ISMEMBERFLG_field=on&ISMEMBERFLG=Y&RANGE=1/10000&NOWEBFLG=%3C%3EY&SHOWSQL=N'
_url = 'https://members.iaee.com/cvweb/cgi-bin/memberdll.dll/List?SORT=ORGNAME&ISMEMBERFLG_field=on&ISMEMBERFLG=N&RANGE=1/59047&NOWEBFLG=%3C%3EY&SHOWSQL=N'
base = urlopen(_url).read()
soup = BeautifulSoup(base, 'html.parser')

member_ids = []
entries = 0

for data in soup.find_all('a'):
	member = str(data.get('onclick'))
	member = re.findall('\d+', member)
	if len(member) != 0:
		member_ids.append(member)

with open('<specifiy .csv file here>', 'w') as csvfile:
	fieldnames = ['First Name', 'Last Name', 'Title', 'Company', 'Email', 'Phone', 'Address']
	w = csv.DictWriter(csvfile, fieldnames=fieldnames)
	w.writeheader()

	for _id in member_ids:
		_id = ''.join(_id)
		urls = "https://members.iaee.com/cvweb/cgi-bin/memberdll.dll/info?customercd={}&wrp=customer_profile.htm".format(_id)
		try:
			base = urlopen(urls).read()
		except:
			print "URL Error avoided.."
		soup = BeautifulSoup(base, 'html.parser')
		for name in soup.find_all('h2'):
			for e in name.find('small'):
					title = e
					e.extract()
					names = name.get_text().split()
					try:
						firstname = names[0]
						lastname = names[1]
					except:
						print ''
					print "First Name: ", firstname
					print "Last Name: ", lastname
					title = title.split(',')
					role = title[0]
					company = title[-1]
					print "Title: ", role
					print "Company: ", company
		# content = soup.find_all('dl', {'class': 'dl-horizontal'})
		phone = soup.findChild('dt', text="Work Phone")
		email = soup.findChild('dt', text="Email Address")
		address = soup.find('address')
		address = address.text.strip()
		address = ' '.join(address.split())
		print "Address: ", address

		try:
			n = phone.findNext('dd').text
			e = email.findNext('dd').text
			print "Phone: ", phone.findNext('dd').text
			print "Email: ", email.findNext('dd').text
		except:
			print '', ''
		try:
			w.writerow({'First Name': firstname, 'Last Name': lastname, 'Title': role, 'Company': company, 'Email': e, 'Phone': n, 'Address': address})
			entries += 1
			print "Entries saved: ", entries
		except:
			pass

