# Scraper for IAEE directory #
Date written: 2016-05-02

This scraper is designed specifically for the IAEE member directory and the 'unique' url it targets.

### Summary ###

* It is designed to search through each of the members listed on the page.
* Access each member's profile.
* Collect the following information: First name / Last name / Company / Role / Email / Phone number / Location
* And save it all into an excel spreadsheet.

### How do I use it? ###

* Clone the repo
* Make a .cvs file
* Make sure the scraper is writing to that file you created
* Run it in the terminal and you're good to go

### Author ###

Noel Colon